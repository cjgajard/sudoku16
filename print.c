#include <stdio.h>
#include "sudoku.h"

static void border_fprint (FILE *f, enum bordertype b)
{
	int i, j;

	switch (b) {
	case BORDERTOP:
		fprintf(f, "┌");
		break;
	case BORDERMIDDLE:
		fprintf(f, "├");
		break;
	case BORDERBOTTOM:
		fprintf(f, "└");
		break;
	}

	for (j = 0; j < SQLEN - 1; j++) {
		for (i = 0; i < 2 * SQLEN + 1; i++) {
			fprintf(f, "─");
		}
		switch (b) {
		case BORDERTOP:
		fprintf(f, "┬");
			break;
		case BORDERMIDDLE:
			fprintf(f, "┼");
			break;
		case BORDERBOTTOM:
			fprintf(f, "┴");
			break;
		}
	}

	for (i = 0; i < 2 * SQLEN + 1; i++) {
		fprintf(f, "─");
	}
	switch (b) {
	case BORDERTOP:
		fprintf(f, "┐");
		break;
	case BORDERMIDDLE:
		fprintf(f, "┤");
		break;
	case BORDERBOTTOM:
		fprintf(f, "┘");
		break;
	}
	fprintf(f, "\n");
}

static void char_fprint (FILE *f, struct cell c)
{
	if (!(c.flags & CELL_SET)) {
		fprintf(f, "  ");
		return;
	}
	if (c.value > 0x10) {
		fprintf(f, "%-2c", 0x30 + c.value);
	} else if (c.value) {
		fprintf(f, "%-2X", c.value - (SQLEN == 4 ? 1 : 0));
	} else {
		fprintf(f, "  ");
	}
}

static void gameline_fprint (FILE *f, int offset)
{
	int i, j, k;
	for (j = 0; j < SQLEN; j++) {
		fprintf(f, "│");
		for (i = 0; i < SQLEN; i++) {
			fprintf(f, " ");
			for (k = 0; k < SQLEN; k++) {
				char_fprint(f, game.board[offset + j * SQLINE + i * SQLEN + k]);
			}
			fprintf(f, "│");
		}
		fprintf(f, "\n");
	}
}

void game_fprint (FILE *f)
{
	int  k;
	border_fprint(f, BORDERTOP);
	gameline_fprint(f, 0);
	for (k = 1; k < SQLEN; k++) {
		border_fprint(f, BORDERMIDDLE);
		gameline_fprint(f, k * SQLEN * SQLINE);
	}
	border_fprint(f, BORDERBOTTOM);
}
