#ifndef SUDOKU_H
#define SUDOKU_H

#define SQLEN 3
#define SQLINE (SQLEN * SQLEN)
#define SQSIZ (SQLEN * SQLEN * SQLEN * SQLEN)
#define DIFFICULTY 0.5
#define CTRL(x) ((x) & 0x1f)

enum bordertype {
	BORDERTOP = 1,
	BORDERMIDDLE,
	BORDERBOTTOM
};

struct options {
	int len;
	unsigned char line[SQLINE];
};

struct cell {
	int flags;
	unsigned char value;
	unsigned char usr;
};

#define CELL_SET (1 << 0)
#define CELL_FIX (1 << 1)

/* Board state */
struct game {
	int cell_count;
	int row, col;
	struct options opts;
	struct cell board[SQSIZ];
};

/* Main board */
extern struct game game;

extern struct options opts;

/* Stack of game states */
struct gamebackup {
	struct gamebackup *next;
	struct game game;
};

void gamebackup_pop (void);
void gamebackup_add (int row, int col);
void gamebackup_destroy (void);

/* Copies of the last game state without guesses */
extern struct gamebackup *backup;

extern int cy, cx; /* cursor */
extern long game_time, game_start_time;

void window_initialize (void);
void window_finalize (void);
void window_refresh (void);
void window_win (void);

enum window_event_type {
	WINDOW_EVENT_QUIT = 1,
	WINDOW_EVENT_MOVE,
	WINDOW_EVENT_MOVETOEMPTY,
	WINDOW_EVENT_REMOVE,
	WINDOW_EVENT_ADD
};
struct window_event {
	enum window_event_type type;
	int dy, dx;
	unsigned char usr;
};
void window_event_get (struct window_event *e);
#endif
