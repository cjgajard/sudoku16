CFLAGS = -ansi -g -Werror -Wall -Wpedantic
LIBS = -lncursesw
OUT = $(shell basename $(CURDIR))
OBJ = gamebackup.o print.o window.o main.o

.PHONY: clean Makeobjects

$(OUT): $(OBJ)
	$(CC) $(CFLAGS) -o $@ $^ $(LIBS)

clean:
	-$(RM) *.o
	-$(RM) $(OUT)

Makeobjects:
	find . -name '*.c' -exec $(CC) -MM {} \; >$@

include Makeobjects
