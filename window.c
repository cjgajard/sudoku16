#include <curses.h>
#include <locale.h>
#include <string.h>
#include "sudoku.h"

#define PAIR_GRAY_ID 1
#define PAIR_GRAY    COLOR_PAIR(PAIR_GRAY_ID)

static void border_wprintw (WINDOW *w, enum bordertype b)
{
	int i, j;
	int x, y;
	getyx(stdscr, y, x);

	attron(PAIR_GRAY);

	switch (b) {
	case BORDERTOP:
		mvwprintw(w, y, x, "┌");
		break;
	case BORDERMIDDLE:
		wprintw(w, "├");
		break;
	case BORDERBOTTOM:
		wprintw(w, "└");
		break;
	}

	for (j = 0; j < SQLEN - 1; j++) {
		for (i = 0; i < 2 * SQLEN + 1; i++) {
			wprintw(w, "─");
		}
		switch (b) {
		case BORDERTOP:
			wprintw(w, "┬");
			break;
		case BORDERMIDDLE:
			wprintw(w, "┼");
			break;
		case BORDERBOTTOM:
			wprintw(w, "┴");
			break;
		}
	}

	for (i = 0; i < 2 * SQLEN + 1; i++) {
		wprintw(w, "─");
	}
	switch (b) {
	case BORDERTOP:
		wprintw(w, "┐");
		break;
	case BORDERMIDDLE:
		wprintw(w, "┤");
		break;
	case BORDERBOTTOM:
		wprintw(w, "┘");
		break;
	}
	attroff(PAIR_GRAY);

	move(++y, x);
}

static void char_wprintw (WINDOW *w, struct cell c)
{
	unsigned char v = c.usr;
	if (!(c.flags & CELL_SET)) {
		wprintw(w, "  ");
		return;
	}
	if (c.flags & CELL_FIX) {
		attron(PAIR_GRAY);
	} else {
		attron(A_BOLD);
	}
	if (c.value > 0x10) {
		wprintw(w, "%-2c", 0x30 + v);
	} else if (c.value) {
		/* 0-f instead of 1-10 when 4x4 */
		wprintw(w, "%-2X", (SQLEN == 4) ? v - 1: v);
	} else {
		wprintw(w, "  ");
	}
	if (c.flags & CELL_FIX) {
		attroff(PAIR_GRAY);
	} else {
		attroff(A_BOLD);
	}
}

static void gameline_wprintw (WINDOW *w, int offset)
{
	int i, j, k;
	int x, y;
	getyx(stdscr, y, x);

	for (j = 0; j < SQLEN; j++) {
		attron(PAIR_GRAY);
		wprintw(w, "│");
		attroff(PAIR_GRAY);
		for (i = 0; i < SQLEN; i++) {
			wprintw(w, " ");
			for (k = 0; k < SQLEN; k++) {
				char_wprintw(w, game.board[offset + j * SQLINE + i * SQLEN + k]);
			}
			attron(PAIR_GRAY);
			wprintw(w, "│");
			attroff(PAIR_GRAY);
		}
		move(++y, x);
	}
}

static void game_wprintw (WINDOW *win)
{
	int  k;
	border_wprintw(win, BORDERTOP);
	gameline_wprintw(win, 0);
	for (k = 1; k < SQLEN; k++) {
		border_wprintw(win, BORDERMIDDLE);
		gameline_wprintw(win, k * SQLEN * SQLINE);
	}
	border_wprintw(win, BORDERBOTTOM);
	refresh();
}

void game_printw (void)
{
	game_wprintw(stdscr);
}

void cell_printw (int y, int x)
{
	char_wprintw(stdscr, game.board[y * SQLINE + x]);
}

void window_initialize (void)
{
	setlocale(LC_ALL, "");
	initscr();
	cbreak();
	noecho();
	keypad(stdscr, TRUE);
	timeout(30);
	/* intrflush(stdscr, FALSE); */
	if (has_colors()) {
		start_color();
		use_default_colors();
		init_pair(PAIR_GRAY_ID, COLOR_WHITE, -1);
	}

	move(0, 0);
}

void window_finalize (void)
{
	endwin();
}

void window_refresh (void)
{
	long t = game_time - game_start_time;
	move(SQLINE + SQLEN + 1, 1);
	if (t >= 3600) {
		printw("%02ld:", t / 3600);
	}
	printw("%02ld:%02ld", t / 60, t % 60);
	move(cy + cy / SQLEN + 1, 2 * (cx + cx / SQLEN + 1));
	refresh();
}

void window_win (void)
{
	mvprintw(SQLINE + SQLEN + 2, 0, "GAME SOLVED");
	getch();
}

void window_event_get (struct window_event *ev)
{
	memset(ev, 0, sizeof(*ev));
	switch (getch()) {
	case CTRL('D'):
		ev->type = WINDOW_EVENT_QUIT;
		break;

	case KEY_LEFT:
	case 'H':
	case 'h':
		ev->type = WINDOW_EVENT_MOVE;
		ev->dx = -1;
		break;
	case KEY_DOWN:
	case 'J':
	case 'j':
		ev->type = WINDOW_EVENT_MOVE;
		ev->dy = 1;
		break;
	case KEY_UP:
	case 'K':
	case 'k':
		ev->type = WINDOW_EVENT_MOVE;
		ev->dy = -1;
		break;
	case KEY_RIGHT:
	case 'L':
	case 'l':
		ev->type = WINDOW_EVENT_MOVE;
		ev->dx = 1;
		break;

	case 'Y':
	case 'y':
		ev->type = WINDOW_EVENT_MOVE;
		ev->dy = -1;
		ev->dx = -1;
		break;
	case 'U':
	case 'u':
		ev->type = WINDOW_EVENT_MOVE;
		ev->dy = 1;
		ev->dx = -1;
		break;
	case 'N':
	case 'n':
		ev->type = WINDOW_EVENT_MOVE;
		ev->dy = -1;
		ev->dx = 1;
		break;
	case 'M':
	case 'm':
		ev->type = WINDOW_EVENT_MOVE;
		ev->dy = 1;
		ev->dx = 1;
		break;

	case CTRL('H'):
		ev->type = WINDOW_EVENT_MOVETOEMPTY;
		ev->dy = 0;
		ev->dx = -1;
		break;
	case CTRL('J'):
		ev->type = WINDOW_EVENT_MOVETOEMPTY;
		ev->dy = 1;
		ev->dx = 0;
		break;
	case CTRL('K'):
		ev->type = WINDOW_EVENT_MOVETOEMPTY;
		ev->dy = -1;
		ev->dx = 0;
		break;
	case CTRL('L'):
		ev->type = WINDOW_EVENT_MOVETOEMPTY;
		ev->dy = 0;
		ev->dx = 1;
		break;

	case CTRL('Y'):
		ev->type = WINDOW_EVENT_MOVETOEMPTY;
		ev->dy = -1;
		ev->dx = -1;
		break;
	case CTRL('U'):
		ev->type = WINDOW_EVENT_MOVETOEMPTY;
		ev->dy = -1;
		ev->dx = 1;
		break;
	case CTRL('N'):
		ev->type = WINDOW_EVENT_MOVETOEMPTY;
		ev->dy = 1;
		ev->dx = -1;
		break;
	case CTRL('M'):
		ev->type = WINDOW_EVENT_MOVETOEMPTY;
		ev->dy = 1;
		ev->dx = 1;
		break;

	case ' ':
	case KEY_BACKSPACE:
		ev->type = WINDOW_EVENT_REMOVE;
		break;
	case '0':
		if (SQLEN == 3) {
			ev->type = WINDOW_EVENT_REMOVE;
			break;
		}
		ev->type = WINDOW_EVENT_ADD;
		ev->usr = 0;
		break;

	case '1':
		ev->type = WINDOW_EVENT_ADD;
		ev->usr = 1;
		break;
	case '2':
		ev->type = WINDOW_EVENT_ADD;
		ev->usr = 2;
		break;
	case '3':
		ev->type = WINDOW_EVENT_ADD;
		ev->usr = 3;
		break;
	case '4':
		ev->type = WINDOW_EVENT_ADD;
		ev->usr = 4;
		break;
	case '5': case 'Q':
		ev->type = WINDOW_EVENT_ADD;
		ev->usr = 5;
		break;
	case '6': case 'W':
		ev->type = WINDOW_EVENT_ADD;
		ev->usr = 6;
		break;
	case '7': case 'E':
		ev->type = WINDOW_EVENT_ADD;
		ev->usr = 7;
		break;
	case '8': case 'R':
		ev->type = WINDOW_EVENT_ADD;
		ev->usr = 8;
		break;
	case '9': case 'A': case 'T':
		ev->type = WINDOW_EVENT_ADD;
		ev->usr = 9;
		break;
	case 'a': case 'S':
		ev->type = WINDOW_EVENT_ADD;
		ev->usr = 10;
		break;
	case 'b': case 'D':
		ev->type = WINDOW_EVENT_ADD;
		ev->usr = 11;
		break;
	case 'c': case 'F':
		ev->type = WINDOW_EVENT_ADD;
		ev->usr = 12;
		break;
	case 'd': case 'Z':
		ev->type = WINDOW_EVENT_ADD;
		ev->usr = 13;
		break;
	case 'e': case 'X':
		ev->type = WINDOW_EVENT_ADD;
		ev->usr = 14;
		break;
	case 'f': case 'C':
		ev->type = WINDOW_EVENT_ADD;
		ev->usr = 15;
		break;
	case 'V':
		ev->type = WINDOW_EVENT_ADD;
		ev->usr = 0;
		break;
	default:
		  break;
	}
}
