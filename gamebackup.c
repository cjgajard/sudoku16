#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sudoku.h"

void gamebackup_pop ()
{
	if (!backup) {
		fprintf(stderr, "no backup\n");
		exit(1);
	}
	game = backup->game;
	backup = backup->next;
}

void gamebackup_add (int row, int col)
{
	struct gamebackup *tail;
	tail = backup;
	backup = malloc(sizeof(*backup));
	backup->next = tail;
	backup->game = game;
	backup->game.row = row;
	backup->game.col = col;
	backup->game.opts = opts;
}

void gamebackup_destroy (void)
{
	while (backup) {
		struct gamebackup *tmp = backup->next;
		free(backup);
		backup = tmp;
	}
}
