#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "sudoku.h"

extern void game_fprint (FILE *f);
extern void game_printw (void);
extern void cell_printw (int y, int x);
struct game game;
struct gamebackup *backup;
struct options opts;

static void line_initialize (void)
{
	int i;
	for (i = 0; i < SQLINE; i++) {
		opts.line[i] = i + 1;
	}
	opts.len = SQLINE;
}

static unsigned char line_pull (struct options *o)
{
	unsigned char tmp;
	int index = rand() % o->len;
	tmp = o->line[index];
	o->line[index] = o->line[--o->len];
	return tmp;
}

static void line_compress (void)
{
	int i = 0;
	while (i < opts.len) {
		unsigned char tmp = 0;
		if (opts.line[i]) {
			i += 1;
			continue;
		}
		do { tmp = opts.line[--opts.len]; } while (tmp == 0 && opts.len > i);
		opts.line[i] = tmp;
		i += 1;
	}
}

static void line_scan_horizontal (int row)
{
	int i;
	for (i = 0; i < SQLINE; i++) {
		unsigned char c = game.board[row * SQLINE + i].value;
		if (!c) {
			continue;
		}
		opts.line[c - 1] = 0;
	}
}

static void line_scan_vertical (int col)
{
	int i;
	for (i = 0; i < SQLINE; i++) {
		unsigned char c = game.board[i * SQLINE + col].value;
		if (!c) {
			continue;
		}
		opts.line[c - 1] = 0;
	}
}

static void line_scan_subsq (int row, int col)
{
	int i, j;
	int o = (row / SQLEN) * SQLEN * SQLINE + (col / SQLEN) * SQLEN;
	for (j = 0; j < SQLEN; j++) {
		for (i = 0; i < SQLEN; i++) {
			int pos = o + j * SQLINE + i;
			unsigned char c = game.board[pos].value;
			if (c) {
				opts.line[c - 1] = 0;
			}
		}
	}
}

void line_debug (void)
{
	int i;
	fprintf(stderr, "[");
	for (i = 0; i < opts.len; i++) {
		fprintf(stderr, "%X", opts.line[i]);
	}
	fprintf(stderr, "] (len:%d)", opts.len);
	fprintf(stderr, "\n");
}

static struct cell *game_scan (int rr, int ro, int cr, int co)
{
	int row, col, min_opts;
	struct cell *ptr = NULL;
	min_opts = SQLINE + 1;
	for (row = ro; row < ro + rr; row++) {
		for (col = co; col < co + cr; col++) {
			int pos = row * SQLINE + col;
			if (game.board[pos].flags & CELL_SET) {
				continue;
			}
			line_initialize();
			line_scan_horizontal(row);
			line_scan_vertical(col);
			line_scan_subsq(row, col);
			line_compress();
			if (opts.len > 0 && opts.len < min_opts) {
				/* Results with less options get priority */
				ptr = game.board + pos;
				min_opts = opts.len;
			}
		}
	}
	return ptr;
}

static struct cell *game_scan_full (void)
{
	return game_scan(SQLINE, 0, SQLINE, 0);
}

static struct cell *game_scan_subsq (int x, int y)
{
	return game_scan(SQLEN, x * SQLEN, SQLEN, y * SQLEN);
}

static int game_add_cell (struct cell *ptr)
{
	int row, col;
	unsigned char c;
	if (!ptr) {
popbackup:
		/* If pointer `ptr` is null we reached a dead end.
		 * Because a guess taken earlier was wrong. We will
		 * recover to the last game backup. */
		gamebackup_pop();
		ptr = game.board + (game.row * SQLINE + game.col);
		row = game.row;
		col = game.col;
		memcpy(&opts, &game.opts, sizeof(opts));
	} else {
		int pos = ptr - game.board;
		row = pos / SQLINE;
		col = pos % SQLINE;
		line_initialize();
		line_scan_horizontal(row);
		line_scan_vertical(col);
		line_scan_subsq(row, col);
		line_compress();
	}
	if (opts.len == 0) {
		goto popbackup;
	}
	c = line_pull(&opts);
	/* NOTE: it is important to call `line_pull` before `gamebackup_add`
	 * because the later will remember the rest of the available options */
	if (opts.len > 0) {
		gamebackup_add(row, col);
	}
	ptr->value = c;
	ptr->usr = c;
	ptr->flags = CELL_SET | CELL_FIX;
	game.cell_count += 1;
	return 0;
}

static int game_initialize (void)
{
	int k;
	game.cell_count = 0;
	for (k = 0; k < SQLEN; k++) {
		int i, j, cur;
		line_initialize();
		cur = k * SQLEN + k * SQLEN * SQLINE;
		for (j = 0; j < SQLEN; j++) {
			for (i = 0; i < SQLEN; i++) {
				unsigned char c;
				int pos = cur + j * SQLINE + i;
				if (pos < 0 || pos >= SQSIZ) {
					fprintf(stderr, "%s:%d out of bounds\n", __FILE__, __LINE__);
				}
				c = line_pull(&opts);
				game.board[pos].flags = CELL_SET | CELL_FIX;
				game.board[pos].value = c;
				game.board[pos].usr = c;
				game.cell_count += 1;
			}
		}
	}
	/* Target cell count is one sub-square more than all diagonals added */
	while (game.cell_count < SQLINE * (SQLEN + 1)) {
		struct cell *ptr;
		ptr = game_scan_subsq(0, 1);
		game_add_cell(ptr);
	}
	/* Fill the rest of the board */
	while (game.cell_count < SQSIZ) {
		struct cell *ptr;
		ptr = game_scan_full();
		game_add_cell(ptr);
	}
	gamebackup_destroy();
	return 0;
}

static void game_start (double difficulty)
{
	int z;
	for (z = 0; z < SQSIZ; z++) {
		if (rand() >= RAND_MAX * difficulty) {
			game.board[z].flags = 0;
			game.board[z].usr = 0;
			game.cell_count -= 1;
		}
	}
}

int cy, cx;
time_t game_start_time, game_time;

void cursor_normalize (void)
{
	cx %= SQLINE;
	cx = cx < 0 ? cx + SQLINE : cx;
	cy %= SQLINE;
	cy = cy < 0 ? cy + SQLINE : cy;
}

void cursor_movetoempty (int dy, int dx)
{
	int i;
	for (i = 0; i <= SQLINE; i++) {
		cy += dy;
		if (cy < 0 || cy >= SQLINE) {
			cy -= dy;
			break;
		}
		cx += dx;
		if (cx < 0 || cx >= SQLINE) {
			cx -= dx;
			break;
		}
		/* Do one extra iteration to stop move into desired direction
		 * when line is completed */
		cursor_normalize();
		/* Stop if cell is empty */
		if (!(game.board[cy * SQLINE + cx].flags & CELL_SET))
			return;
	}
	cursor_normalize();
}

int main ()
{
	unsigned long seed = 4;
	struct window_event ev;
	/* seed = time(NULL); */
	/* fprintf(stderr, "Started with seed %lx\n", seed); */
	srand(seed);

	game_initialize();
	/* DEBUG */ game_fprint(stderr);
	game_start(DIFFICULTY);

	window_initialize();
	game_printw();
	cx = cy = SQLINE / 2;

	game_time = game_start_time = time(NULL);

mainloop:
	window_refresh();
	window_event_get(&ev);
	game_time = time(NULL);

	switch (ev.type) {
	case WINDOW_EVENT_QUIT:
		goto shutdown;
	case WINDOW_EVENT_MOVE:
		cy += ev.dy;
		cx += ev.dx;
		cursor_normalize();
		goto mainloop;
	case WINDOW_EVENT_MOVETOEMPTY:
		cursor_movetoempty(ev.dy, ev.dx);
		goto mainloop;
	case WINDOW_EVENT_REMOVE:
		goto delete;
	case WINDOW_EVENT_ADD:
		break;
	default:
		goto mainloop;
	}

/*add:*/
	switch (SQLEN) {
	case 3:
		if (ev.usr < 1 || ev.usr > 9)
			goto mainloop;
		break;
	case 4:
		ev.usr += 1; /* 0-f to fit within 1 character */
	}
	if (game.board[cy * SQLINE + cx].flags & CELL_FIX) {
		goto mainloop;
	}
	if (!(game.board[cy * SQLINE + cx].flags & CELL_SET)) {
		game.cell_count += 1;
	}
	game.board[cy * SQLINE + cx].flags = CELL_SET;
	game.board[cy * SQLINE + cx].usr = ev.usr;
	cell_printw(cy, cx);

/* checkwin: */
	if (game.cell_count == SQSIZ) {
		int p;
		for (p = 0; p < SQSIZ; p++) {
			struct cell c = game.board[p];
			if (c.flags & CELL_FIX) {
				continue;
			}
			if (!(c.flags & CELL_SET)) {
				/* DEBUG */ fprintf(stderr, "position %d is not set\n", p);
				goto mainloop;
			}
			if (c.usr != c.value) {
				goto mainloop;
			}
		}
		window_win();
		goto shutdown;
	}
	goto mainloop;

delete:
	{
		struct cell c = game.board[cy * SQLINE + cx];
		if (c.flags & CELL_FIX || !(c.flags & CELL_SET)) {
			goto mainloop;
		}
	}
	game.board[cy * SQLINE + cx].flags &= ~CELL_SET;
	game.cell_count -= 1;
	cell_printw(cy, cx);
	goto mainloop;

shutdown:
	window_finalize();
	return 0;
}
